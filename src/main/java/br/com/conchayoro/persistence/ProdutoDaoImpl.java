package br.com.conchayoro.persistence;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.conchayoro.entity.Produto;

public class ProdutoDaoImpl implements ProdutoDao{

	@Inject
	private EntityManager entityManager;
  
	public void incluir(Produto produto){
 
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(produto);
		this.entityManager.getTransaction().commit();
	}
 
	public void alterar(Produto produto){
 
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(produto);
		this.entityManager.getTransaction().commit();
	}
	
	public void excluir(Long codigo){
		 
		Produto produto = this.consultar(codigo);
 
		this.entityManager.getTransaction().begin();
		this.entityManager.remove(produto);
		this.entityManager.getTransaction().commit();
 
	}
 
	@SuppressWarnings("unchecked")
	public List<Produto> listar(){
 
		return this.entityManager.createQuery("SELECT p FROM Produto p ORDER BY p.nome").getResultList();
	}
 
	public Produto consultar(Long codigo){
 
		return this.entityManager.find(Produto.class, codigo);
	}
 
}